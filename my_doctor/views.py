from sqlalchemy import create_engine
import xmpp
from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
from .models import Doctor
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import get_query_entities, get_column_key
engine = create_engine('sqlite:///:memory:', echo=True)
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()
Doc = {'name': 'Iliyaz', 'status': 'free', 'password': 'pass12345', 'chat_id': '12345'}
Pat = {'name': 'Timur', 'status': 'not free', 'password': 'pass112233', 'chat_id': '12345', 'date': ''}
Doctors = [Doc]
Patsients = [Pat]


def messageCB(sess, mess):
    print 'MESSAGE' * 100
    nick = mess.getFrom().getResource()
    text = mess.getBody()
    print text, nick

# @view_config(route_name='registeration', renderer='templates/reg_form.jinja2')
# def registration(request):
#     if request.method == 'POST':
#         name = request.POST.get('name')
#         fullname = request.POST.get('fullname')
#         password = request.POST.get('password')
#         Doctors.append([name, fullname, password])
#         doc = Doctor(id='2', name=name, fullname=fullname, password=password)
#         session.add(doc)
#         print(doc)
#     return {'doc': 'sdfghj'}


@view_config(route_name='doc_pat', renderer='templates/doctor.jinja2')
def doc_pat(request):
    user = request.matchdict['u_name']
    doc = Doctors
    pat = Patsients
    return {'doc': doc, 'pat': pat, 'user': user}


@view_config(route_name='login_function', renderer='templates/login.jinja2')
def login_function(request):
    if request.method == 'POST':
        u_name = request.POST.get('name')
        passwd = request.POST.get('password')
        for doc in Doctors:
            if doc['name'] == u_name and doc['password'] == passwd:
                return HTTPFound(location='/doc_pat/%s' % u_name)
        for pat in Patsients:
            if pat['name'] == u_name and pat['password'] == passwd:
                return HTTPFound(location='/doc_pat/%s' % u_name)
    return {'sdasd': 'asda'}


@view_config(route_name='doc_admin_panel', renderer='templates/doc_admin_panel.jinja2')
def doc_admin_panel(request):
    # user = request.matchdict['u_name']

    doc = Doctors
    pat = Patsients
    return { 'doc': doc, 'pat': pat}


@view_config(route_name='pat_admin_panel', renderer='templates/pat_admin_panel.jinja2')
def pat_admin_panel(request):
    user = request.matchdict['u_name']
    chat_id = request.POST.get('chat_id')
    pat = Patsients
    doc = Doctors
    for doctor in doc:
        doctor[chat_id] = 'doc-' + str(chat_id)
    for patsient in pat:
        patsient[chat_id] = 'doc-' + str(chat_id)
    return {'name': user, 'pat': pat, 'doc': doc}


@view_config(route_name='chat_room', renderer='templates/chat_room.jinja2')
def chat_room(request):
    return {'chat_room_id': 123456}


@view_config(route_name='patsient', renderer='templates/patsient.jinja2')
def patsient(request):
    for doc in session.query(Doctor):
        print(doc.doctor.name)
    return {'sdas': 'sadasd'}


@view_config(route_name='send_msg')
def send_message(request):
    if request.method == 'POST':
        message_text = request.POST.get('message_text')
        tojid = 'timurbek@xmpp.jp'
        text = message_text

        jidparams = {
            'jid': 'iliyaz@xmpp.jp',
            'password': 'isomasihhudo7',
        }
        jid = xmpp.protocol.JID(jidparams['jid'])
        cl = xmpp.Client(jid.getDomain(), debug=[])

        con = cl.connect()
        if not con:
            print 'could not connect!'
            sys.exit()
        print 'connected with', con
        auth = cl.auth(jid.getNode(), jidparams['password'], resource=jid.getResource())
        if not auth:
            print 'could not authenticate!'
            sys.exit()
        print 'authenticated using', auth
        id = cl.send(xmpp.protocol.Message(tojid, text))
        print 'sent message with id', id

        cl.RegisterHandler('message', messageCB)

        while 1:
            cl.Process(1)
        # cl.disconnect()
    return HTTPFound('/doc_chat_room')