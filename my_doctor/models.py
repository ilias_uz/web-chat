from sqlalchemy import Column, Integer, String, Sequence, ForeignKey, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from sqlalchemy.orm import relationship
Base = declarative_base()



class Doctor(Base):
    __tablename__ = 'doctor'
    id = Column(Integer, Sequence('doc_id'), primary_key=True)
    name = Column(String(50))
    fullname = Column(String(50))
    password = Column(String(12))

    # doctor_who_is_id = Column(Integer, ForeignKey("check_who.id"))
    # doctor_who_is = relationship("Address", foreign_keys=[doctor_who_is_id])

    def __repr__(self):
        return "<User(name='%s', fullname='%s', password='%s')>" % (
                                self.name, self.fullname, self.password)


class Patsient(Base):
    __tablename__ = 'patsient'
    id = Column(Integer, Sequence('patsient_id'), primary_key=True)
    name = Column(String(50))
    fullname = Column(String(50))
    password = Column(String(12))
    # patsient_who_is_id = Column(Integer, ForeignKey("check_who.id"))
    # patsient_who_is = relationship("Address", foreign_keys=[patsient_who_is_id])

    def __repr__(self):
        return "<User(name='%s', fullname='%s', password='%s')>" % (
                                self.name, self.fullname, self.password)


class Check_who(Base):
    __tablename__ = 'check_who'
    id = Column(Integer, Sequence('chech_who_id'), primary_key=True)
    name = Column(String(50))
