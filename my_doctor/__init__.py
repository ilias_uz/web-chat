from pyramid.config import Configurator


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)
    config.include('pyramid_chameleon')
    config.include('pyramid_jinja2')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_static_view('stylesheets', 'static/css')
    config.add_static_view('javascript', 'static/js')
    config.add_route('doc_pat', '/doc_pat/{u_name}')
    config.add_route('patsient', '/patsient')
    config.add_route('registeration', '/reg')
    config.add_route('login_function', '/')
    config.add_route('chat_room', '/pat_chat_room')
    config.add_route('send_msg', '/send_msg')
    config.add_route('doc_admin_panel', '/doc_chat_room')
    config.add_route('pat_admin_panel', '/pat_admin/{u_name}')
    config.scan()
    return config.make_wsgi_app()
