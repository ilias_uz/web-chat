pyramid
pyramid_chameleon
pyramid_debugtoolbar
waitress
sqlalchemy
sqlalchemy-utils

[testing]
WebTest >= 1.3.1
pytest
pytest-cov
